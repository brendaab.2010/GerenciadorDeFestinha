/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import view.TelaFestaController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import gerenciadordefestas.Controller;

/**
 * FXML Controller class
 *
 * @author Aluno
 */

public class TelaController extends Controller{

    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    
    @FXML
    public void criarFesta() {      
        this.stage.setScene(mostraTelaFesta());
        System.out.println("Uhuuuuu");
        this.stage.setFullScreen(false);
    }
    
    private Scene mostraTelaFesta() {
             Scene scene = null;
        try {
            System.out.println("Aeeee");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaFestaController.class.getResource("TelaFesta.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
            TelaFestaController cc = loader.getController();
            cc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
    @FXML
    public void venderIngresso() {      
        this.stage.setScene(mostrarTelaIngresso());
        this.stage.setFullScreen(false);
    }
    
    private Scene mostrarTelaIngresso() {
             Scene scene = null;
        try {
            // Loader
            System.out.println("Aeeee");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaIngressosController.class.getResource("TelaIngressos.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            TelaIngressosController cc = loader.getController();
            cc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
    
    @FXML
    public void abrirPontuacao() {      
        this.stage.setScene(mostrarPontuacao());
        this.stage.setFullScreen(false);
    }
    
    private Scene mostrarPontuacao() {
             Scene scene = null;
        try {
            // Loader
            System.out.println("Aeeee");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaPontuaçãoController.class.getResource("TelaPontuação.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            TelaPontuaçãoController cc = loader.getController();
            cc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }  
    
    
   
}


