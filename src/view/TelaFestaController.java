/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import model.Arrays;
import model.Festa;
import gerenciadordefestas.Arquivo;
import gerenciadordefestas.Controller;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Brenda
 */
public class TelaFestaController extends Controller {

    @FXML
    private TextField descricaoFesta;
    
    @FXML
    private TextField nomeFesta;
    
    @FXML
    private TextField capacidadeFesta;
    
    @FXML
    private TextField precoIngresso;
    
    @FXML
    private TextField categoria;
    
    @FXML
    private TextField data;
    

    
  
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    public void criar () {
        Festa festa = new Festa();
          
        festa.setNome(nomeFesta.getText());
        festa.setDescricao(descricaoFesta.getText());
        festa.setCategoria(categoria.getText());
        festa.setData(data.getText());
        festa.setQnt(Integer.parseInt(capacidadeFesta.getText()));
        festa.setPrecoIngresso(Double.parseDouble(precoIngresso.getText()));
        
        
        
        Arrays.festas.add(festa);
        Arquivo.saveFestas(Arrays.festas);
 
        voltar();
    }
  
    

    
     @FXML
    public void voltar() {      
        this.stage.setScene(mostrarTelaAnterior());
        this.stage.setFullScreen(false);
    }
    
    private Scene mostrarTelaAnterior() {
             Scene scene = null;
        try {
            // Loader
            System.out.println("Aeeee");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaController.class.getResource("Tela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            TelaController cc = loader.getController();
            cc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
    
    
}
