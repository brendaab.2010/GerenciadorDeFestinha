/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import model.Arrays;
import model.Cliente;
import model.Festa;
import gerenciadordefestas.Controller;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Brenda
        */
public class TelaIngressosController extends Controller {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private ListView<Festa> festaList; 
    
    private ObservableList<Festa> obsFestaList;
    
    @FXML
    private Label nomeLabel;
    
    @FXML
    private Label descricaoLabel;

    @FXML
    private Label categoriaLabel;    

    @FXML
    private Label dataLabel;     
    
    @FXML
    private Label ingressoLabel;
    
    @FXML
    private Label quantidadeLabel;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    
        obsFestaList = festaList.getItems();
        
          for(Festa f: Arrays.festas){
                obsFestaList.add(f);
            }
          
        this.mostraItemComLambda();
        this.cellFactory();
            
    }    
    
    
        @FXML
    public void vender() {     
        
        Festa selectedItem = festaList.getSelectionModel().getSelectedItem();
        //Não sei oq fazer aqui
      //  Cliente.ing.add(selectedItem.getIngresso());
        if (selectedItem != null) {
            TelaCompraController tcc = new TelaCompraController();
            tcc.informarFesta(selectedItem);
            this.stage.setScene(mostrarTelaCompra());
        
        this.stage.setFullScreen(false);
         
        } else {
            // Nothing selected.
            System.out.println("SELECIONA ALGO");
        }
    }
    

        
    private void cellFactory() {
        festaList.setCellFactory(param -> new ListCell<Festa>() {
            @Override
            protected void updateItem(Festa f, boolean empty) {
                super.updateItem(f, empty);
                
                if (empty) {
                    setText(null);
                } else {
                   setText(f.getNome());

                }
            }
        });
    }

    private void mostraItemComLambda() {
        festaList.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    showItemDetails(observable.getValue());

                });
    }

        private void showItemDetails(Festa f) { // Aqui, nas labels que não tinham nada, agora tem o conteudo do banco de daods
            

            if(f != null){
            
           nomeLabel.setText(f.getNome());
           descricaoLabel.setText(f.getDescricao());
           categoriaLabel.setText("Categoria: "+f.getCategoria());
           dataLabel.setText("Data: " +f.getData());
           ingressoLabel.setText("O ingresso é R$ "+ Double.toString(f.getPrecoIngresso()));
           quantidadeLabel.setText("Quantidade de ingresso vendidos: "+Integer.toString(f.ingressoFesta.size()));

        }
    }
    
        
        
        
        
    
    private Scene mostrarTelaCompra() {
             Scene scene = null;
        try {
            // Loader
            System.out.println("Aeeee");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaCompraController.class.getResource("TelaCompra.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            TelaCompraController cc = loader.getController();
            cc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
         @FXML
    public void voltar() {      
        this.stage.setScene(mostrarTelaAnterior());
        this.stage.setFullScreen(false);
    }
    
    private Scene mostrarTelaAnterior() {
             Scene scene = null;
        try {
            // Loader
            System.out.println("Aeeee");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaController.class.getResource("Tela.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            TelaController cc = loader.getController();
            cc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
}
