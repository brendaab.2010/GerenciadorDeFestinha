/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Brenda
 */
public class Festa  implements Serializable{
    
    private String nome;
    private String descricao;
    private String categoria;
    public  List<Ingresso> ingressoFesta = new ArrayList<>();
    private String data;
    private int qnt;
    private double precoIngresso;

    
    
    public void setPrecoIngresso(double preco) {
        precoIngresso =preco;
    }
    
    public double getPrecoIngresso(){       
        return precoIngresso;
    }
    
    
    public int getQnt() {
        return qnt;
    }

    public void setQnt(int qnt) {
        this.qnt = qnt;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Ingresso getIngresso() {
        Ingresso ingresso = new Ingresso();
        ingressoFesta.add(ingresso);
        ingresso.setPreco(this.precoIngresso);
        return ingresso;
    }
    
}
