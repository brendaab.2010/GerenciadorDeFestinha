/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Brenda
 */
public class Cliente  implements Serializable{
    
    private String nome;
    private String email;
    private String sexo;
    public List<Ingresso> ing = new ArrayList<>();
    private int codigo;
    private int pontuacao;

 
    
    public int getPontuacao() {   
        return (ing.size() * 15);
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public List<Ingresso> getIng() {
        return ing;
    }

    public void addIngresso(Ingresso i) {
        ing.add(i);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    
}
