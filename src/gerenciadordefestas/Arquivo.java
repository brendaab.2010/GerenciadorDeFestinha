package gerenciadordefestas;

import model.Cliente;
import model.Festa;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;



public class Arquivo {
    
    
        public static boolean saveCliente(List<Cliente> f) {
        
        try {
            FileOutputStream fos = new FileOutputStream("clientes.bld", false);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(f);
            oos.flush();
            oos.close();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
       
    public static boolean saveFestas(List<Festa> fes) {
        
        try {
            FileOutputStream fos = new FileOutputStream("festas.bld", false);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(fes);
            oos.flush();
            oos.close();
        } catch(Exception e) {
            // e.printStackTrace();
            return false;
        }
        
        return true;
    }
    

    public static List<Cliente> loadCliente () {
         
        List<Cliente> cli;
        
        try {
            InputStream fos = new FileInputStream("clientes.bld");
            ObjectInputStream ois = new ObjectInputStream(fos);
            cli = (List<Cliente>) ois.readObject();
            fos.close();
            ois.close();
            
        } catch (Exception e) {
             e.printStackTrace();
            return new ArrayList<Cliente>();
        }
        
        return cli;
    }
    
    
    public static List<Festa> loadFesta () {
        List<Festa> fes;
        
        try {
            InputStream fos = new FileInputStream("festas.bld");
            ObjectInputStream ois = new ObjectInputStream(fos);
            fes = (List<Festa>) ois.readObject();
            fos.close();
            ois.close();
            
        } catch (Exception e) {
             e.printStackTrace();
            return new ArrayList<Festa>();
        }
        
        return fes;
    }

}