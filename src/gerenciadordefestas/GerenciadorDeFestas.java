package gerenciadordefestas;

import model.Arrays;
import model.Festa;
import view.TelaController;
import java.io.IOException;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 */

public class GerenciadorDeFestas extends Application {
    
    private Stage primaryStage; // Declaração do stage 
    public static Stage ps;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage; // A nossa classe main recebe um stage como parametro, o Stage é o container principal que é geralmente uma janela com uma borda e os botões típicos minimizar, maximizar e fechar.
        primaryStage.setTitle("Gerenciador de Festas"); //Nós damos um título pra ele
        primaryStage.setScene(initLayout()); // e mandamos uma Scene (cena) acontecer
        primaryStage.show(); // Mostramos nossa stage
        ps = primaryStage;
    }
    
    /**
     * Initializes the scene.
     * @return scene
     */
    public Scene initLayout() { // Dentro da Scene (cena) os componentes do JavaFX como AnchorPane, TextBox, etc. são adicionados.
        Scene scene = null; //Ela começa nula
        try {
            FXMLLoader loader = new FXMLLoader(); // definimos a variavel loader como a carregadora de fxml
            loader.setLocation(TelaController.class.getResource("Tela.fxml")); // Carregamos o arquivo fxml de nome "Loja" e dizemos que quem controla ele é a classe "LojaController"
            AnchorPane layout = (AnchorPane) loader.load(); // A variavel layout do tipo AchorPane recebe o AnchorPane no nosso arquivo fxml  

            TelaController cc = loader.getController();
            cc.setStage(primaryStage);
                    
            
            scene = new Scene(layout); // Mostra a cena com o layout que acabamos de carregar.
        } catch (IOException e) {
            System.out.println("Exception de fxml"); // Se ele não conseguir carregar, irá aparecer Exception de fxml na saida
            e.printStackTrace();
        }
        return scene; //Ele retorna a cena dele para carregarmos no método start
    }

    /**
     * Returns the main stage.
     * @return primaryStage
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     Arrays.clientes = Arquivo.loadCliente();
     Arrays.festas = Arquivo.loadFesta();
     launch(args);
      
        
    }
}
